# Netzwerkeinstellungen
Konfiguration von DHCP, Interfaces, LAN und WLAN. 

## Netzwork-Interfaces
Verfügbare Netzwerkschnittstellen anzeigen
```sh
ip link
```
Netzwerkschnittstelle aktivieren|deaktivieren
```sh
ip link set "interface" up|down
```
Status einer Netzwerkschnittstelle anzeigen
```sh
ip link show dev "interface"
```

## WLAN
In `/etc/wpa_supplicant/wpa_supplicant.conf`
> ctrl_interface=/run/wpa_supplicant  
> update_config=1

eintragen
WPA_Supplicant auf dem Interface und mit der Konfigurationfile starten
```sh
wpa_supplicant -B -i "interface" -c /etc/wpa_supplicant/wpa_supplicant.conf
```
Das WPA_Supplicant Command-Line-Interface für das Interface starten
```sh
wpa_cli -i "interface"
```
Im `wpa_cli` folgende Befehle ausführen.  
Mit `scan` nach verfügbaren WLANs suchen und mit `scan_results` die Ergebnisse anzeigen lassen. `add_network` wählt eines der Ergebnisse aus
```sh
scan
scan_results
add_network
set_network 0 ssid "mySSID"
set_network 0 psk "password"
enable_network 0
save_config
```
### Eduroam
Benötigte Pakete python python-dbus networkmanager plasma-nm  
Bei python und python-dbus bin ich mir nicht sicher

## DHCPCD
dhcpcd für ein bestimmtes Interface beim Start des Rechners aktivieren und sofort starten
```sh
systemctl enable --now dhcpcd@"interface".service
```