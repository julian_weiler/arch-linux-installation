# Arch Linux Post Installation Script

## Default Shell
Wechsle die default Shell fuer *root* auf zsh
```sh
chsh -s /bin/zsh
```

## Benutzer hinzufuegen
Lege einen neuen Benutzer *jweiler* samt Home-Verzeichnis in der Gruppe *wheel* an und weise ihm *zsh* als default Shell zu. Dann setze das Passwort fuer den Benutzer.
```sh
useradd -m -G wheel -s /bin/zsh jweiler
passwd jweiler
```

## sudo fuer Gruppe wheel
mit visudo die `/etc/sudoers` oeffnen und in der Zeile # %wheel   ALL=(ALL) ALL das # entfernen