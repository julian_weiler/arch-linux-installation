# Arch Linux Installation
Installation des Grundsystems sowie aller benötigten Pakete, konfiguration des Systems.

1. Grundsystem installieren
2. Post Konfiguration vornehmen
    1. Shell wechslen
    2. User einrichten
    3. sudo konfigurieren
3. Netzwerk einrichten
4. GUI installieren
5. zusätzliche Tools installieren