# Arch Linux Installation Script

## Tastaturbelegung aendern
```sh
loadkeys de		#deutsche Tastaturlayout ...
loadkeys de-latin1	#... mit Akzenttasten
```
## Partitionierung und Verschluesselung
### Erstellen einer EFI Partition
Die EFI Partition darf nicht mit LVM erstellt werden
```sh
fdisk /dev/sda
```
Alte Partitionen loeschen (d)  
Neue Partition erstellen (n; +1G)  
Typ der neuen Partition nach EFI Partition aendern (t; type=1)  
Aenderungen auf der Festplatte schreiben (w)

### Formatieren der Partition mit FAT32
```sh
mkfs.fat -F32 /dev/sda1
```

### Erstellen einer Linux Filesystem Partition
```sh
cfdisk #Partition nimmt den restlichen Speicherplatz der Festplatte ein
```

### Ueberschreiben der Partition
```sh
shred -v -n 1 /dev/sda2
```

### LVM on LUKS
```sh
modprobe dm-crypt	#Kernel-Modul fuer die Verschluesselung laden
cryptsetup -c aes-xts-plain64 -y -s 512 luksFormat /dev/sda2	#sda2 verschluesseln
```

### SSD
#+----------------------------------------------+
#|	efi partition {/dev/sda1} {fat32}		   	|
#|	+-----------------------------------------+	|
#|	|										  | |
#|	+-----------------------------------------+	|
#| 											   	|
#|	physical volume	{/dev/sda2} {encrypted_lvm}	|
#|	+-----------------------------------------+	|
#|	|										  | |
#|	|	vg_arch							      | |
#|	|	+-----------------------------------+ | |
#|	|	|								   	| | |
#|	|	|	lv_root {ext4}				   	| | |
#|	|	|	+-----------------------------+ | | |
#|	|	|	|							  | | | |
#|	|	|	+-----------------------------+ | | |
#|	|	|								   	| |	|
#|	|	|	lv_swap	{swap}				   	| |	|
#|	|	|	+-----------------------------+ | | |
#|	|	|	|							  |	| | |
#|	|	|	+-----------------------------+ | | |
#|	|	|								   	| | |
#|	|	|	lv_home	{ext4}				   	| | |
#|	|	|	+-----------------------------+ | | |
#|	|	|	|							  |	| | |
#|	|	|	+-----------------------------+ | | |
#|	|	+-----------------------------------+ | |
#|	+-----------------------------------------+ |
#+----------------------------------------------+

### LVM Setup
```sh
cryptsetup luksOpen /dev/sda2 encrypted_lvm	#Verschluesselte Partition oeffnen
pvcreate /dev/mapper/encrypted_lvm		#Physical Volume
vgcreate vg_arch /dev/mapper/encrypted_lvm	#Volume Group: vg_arch
lvcreate -L 50GB -n lv_root vg_arch		#Logical Volume: lv_root
lvcreate -L 16GB -n lv_swap vg_arch		#Logical Volume: lv_swap
lvcreate -l 100%FREE -n lv_home vg_arch		#Logical Volume: lv_home
```

### Partitionen formatieren
```sh
mkfs.ext4 -L p_root /dev/mapper/vg_arch-lv_root	#
mkfs.ext4 -L p_home /dev/mapper/vg_arch-lv_home	#
mkswap -L p_swap /dev/mapper/vg_arch-lv_swap	#
```

### Partitionen mounten
```sh
mount -L p_root /mnt
mkdir /mnt/home
mount -L p_home /mnt/home
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
```

### Swap-Partition aktivieren
```sh
swapon -L p_swap
```

## Serverliste filtern und nur Server aus Deutschland behalten
```sh
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
grep -E -A 1 ".*Germany.*$" /etc/pacman.d/mirrorlist.bak | sed '/--/d' > /etc/pacman.d/mirrorlist
```

## Basis-System installieren
"base" und "base-devel" : Basissystem  
"zsh" : Terminal  
"wpa_supplicant", "dialog", "wireless_tools" : WIFI  
"intel_ucode" : Microcode Updates für die CPU  
"grub", "efibootmgr", "dosfstools", "gptdisk" : Bootloader (mit systemd-boot nicht benoetigt?)

```sh
pacstrap /mnt base base-devel zsh wpa_supplicant dialog wireless_tools intel-ucode
```

## fstab generieren
Die fstab mit der Option fuer UUIDs generieren
```sh
genfstab -Up /mnt > /mnt/etc/fstab
```

## fstab anpassen
```sh
blkid -o list
```
In `/mnt/etc/fstab` mit nano die UUIDs eintragen und die Anpassungen fuer SSD vornehmen
```sh
nano /mnt/etc/fstab
```
> /dev/mapper/vg_arch-lv_root UUID=`<UUID1>`  
> UUID=`<UUID1>` / ext4	rw,defaults,noatime,discard	0 1  
> /dev/mapper/vg_arch-lv_home UUID=`<UUID2>`  
> UUID=`<UUID2>` /home ext4	rw,defaults,noatime,discard	0 2  
> /dev/sda1 UUID=`<UUID3>`  
> UUID=`<UUID3>` /boot vfat	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro	0 2  
> /dev/mapper/vg_arch-lv_swap UUID=`<UUID4>`  
> UUID=`<UUID4>` none swap defaults,noatime,discard	0 0


## Nach chroot wechseln
```sh
arch-chroot /mnt
```

## Rechnername festlegen
```sh
echo laptop1 > /etc/hostname
```

## Spracheinstellungen festlegen
```sh
echo LANG=de_DE.UTF-8 > /etc/locale.conf
```

## In /etc/locale.gen '#' am Anfang folgender Zeilen suchen und entfernen:
```sh
de_DE.UTF-8 UTF-8
de_DE ISO-8859-1
de_DE@euro ISO-8859-15
en_US.UTF-8
```

## locales generieren
```sh
locale-gen
```

## ...
```sh
echo KEYMAP=de-latin1 > /etc/vconsole.conf
echo FONT=lat9w-16 >> /etc/vconsole.conf
```

## Symlink fuer die Zeitzone erstellen
```sh
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
```

## Hosts-Datei editieren
```sh
echo 127.0.0.1 localhost localhost >> /etc/hosts
```

## Kernel-Module und -Hooks anpassen und Kernel-Image erzeugen
In `/etc/mkinitcpio.conf` die Module und Hooks anpassen
> Modules=(ext4)  
> HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt sd-lvm2 filesystems fsck)
```sh
mkinitcpio -p linux	#Kernel-Image erstellen
```
missing firmware for modules wd719x (Driver for Western Digital WD7193, WD7197 and WD7296 SCSI cards) and aic94xx (Adaptec SAS 44300, 48300, 58300 Sequencer Firmware for AIC94xx driver) can be ignored

## install systemd-boot into the EFI system partition (also /boot)
```sh
bootctl --path=/boot install
```

## automatic update for efi boot manager in /etc/pacman.d/hooks/systemd-boot.hook
```sh
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
```

## configuration
UUID5 ist die /dev/sda2 partition und nicht encrypted_lvm!!!!!!!!!!!!!!!!!!!!!!  
Create `/boot/loader/entries/arch.conf`
> title Arch Linux  
> linux	/vmlinuz-linux  
> initrd /intel-ucode.img		# load microcode updates  
> initrd /initramfs-linux.img  
> options rd.luks.name=`<UUID5>`=encrypted_lvm resume=/dev/mapper/vg_arch-lv_swap root=/dev/mapper/vg_arch-lv_root rw quiet

## Anpassen der loader Konfiguration in /boot/loader/loader.conf
```sh
timeout	3
default arch
editor	no
```

## Root Passwort setzen
```sh
passwd
```

## chroot verlassen, Partitionen entmounten und den Rechner rebooten
```sh
exit
umount {p_home, p_boot, p_root}
reboot
```