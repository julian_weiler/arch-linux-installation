# Graphical User Interface installieren

## X-Server
```sh
pacman -Ss xf86-video-intel mesa lib32-mesa
```

## KDE
### Metapaket
Das Metapaket ```plasma-meta``` behandelt die Komponenten der Desktopumgebung als Abhängigkeiten. Mit der Installation eines Meta-Pakets wird sichergestellt, dass evt. später neu hinzukommende Komponenten bei einem Systemupdate automatisch nachinstalliert werden. 
```sh
pacman -S plasma-meta
```
### Sprachpaket
Das deutsche Sprachpaket ```kde-l10n-de```
```sh
pacman -S kde-l10n-de
```
### KDE Applications als Metapaket
```sh
pacman -S kde-applications-meta
```

## Displaymanager sddm
```sh
pacman -S sddm sddm-kcm
```
systemd service beim startup aktivieren und sofort starten
```sh
systemctl enable --now sddm.service
```

## Wayland
```sh
pacman -S plasma-wayland-session
```