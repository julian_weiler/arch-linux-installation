# Tools
Verschiedene Programme installieren und konfigurieren

## WireGuard
systemd-networkd hat nativen Support des WireGuard Protokolls und benoetigt deshalb ```wireguard-tools``` als Paket nicht
```sh
pacman -S wireguard-arch
```

## Firefox
Installiere Firefox und das deutsche Sprachpaket
```sh
pacman -S firefox firefox-i18n-de
```
### Multimedia playback
FFFmpeg und PulseAudio
```sh
pacman -S ffmpeg pulseaudio
```
### KDE Integration
To bring the KDE look to GTK apps (including Firefox), install breeze-gtk and kde-gtk-config. Afterwards, go to System Settings > Application Style > GNOME/GTK Application Style. Be sure to choose 'Breeze' in 'Select a GTK2/GTK3 Theme' and check 'Show icons in GTK buttons' and 'Show icons in GTK menus'.
```sh
pacman -S breeze-gtk kde-gtk-config
```
### Plasma Integration
Bessere Integration in KDE Plasma 5 (benoetigt das Firefox Addon [Plasma Integration](https://addons.mozilla.org/firefox/addon/plasma-integration/))
```sh
pacman -S plasma-browser-integration
```

## KeePassXC
```sh
pacman -S keepassxc
```
[KeePassXC Firefox Plugin](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser/)

## VLC
```sh
pacman -S vlc
```

## Atom
```sh
pacman -S atom
```
### Installieren von Plugins ueber die Shell
```sh
apm install *paketname*
```

## Byobu
```sh
pacman -S byobu
```

## JetBrains Toolbox
```sh
pacman -S jetbrains-toolbox
```

## ToDo
git